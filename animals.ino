

#include <InternetButton.h>





#include "math.h"
InternetButton b = InternetButton();


// Global variables to store how many lights should be turned on
int optionPressed = 1;
bool numLightsChanged = false;
int score;

void setup() {

    // 1. Setup the Internet Button
    b.begin();
score = 0;
   
    Particle.function("animals", BtnPressed);
 
    
  
}

void loop(){
    
    // This loop just sits here and waits for the numLightsChanged variable to change to true
    // Once it changes to true, run the activateLEDS() function.
    if(numLightsChanged == true){
        delay(10);
        activateLEDS();
       
        numLightsChanged = false;
    }
}

 void activateLEDS(){

    // 1. turn off all lights
    b.allLedsOff();
    
   if(optionPressed == 1){
       
        b.allLedsOn(0,255, 0);
                       score += 1;
                     Particle.publish("score", String(score));

    }
    
   if(optionPressed == 2){
       
         b.allLedsOn(255, 0, 0);
    }
 }
 
 int BtnPressed(String command){
    //parse the string into an integer
    int Correct = atoi(command.c_str());

    
    // If no errors, then set the global variable to numLights
    optionPressed = Correct;

    numLightsChanged = true;

    // In embedded programming, "return 1" means that 
    // the function finished successfully
    return 1;
}